﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace projeto_v0
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
               "Cadastro_Atividades",
               "Home/Atividade/Index",
               new { controller = "Atividade", action = "Index" }
               );

            routes.MapRoute(
               "Lista_Usuários",
               "Home/Usuario/Index",
               new { controller = "Usuario", action = "Index" }
               );
            routes.MapRoute(
               "Cadastro_Usuários",
               "Home/Usuario/Create",
               new { controller = "Usuario", action = "Create" }
               );

            routes.MapRoute(
               "Lista_Alunos",
               "Home/Participante/Index/{id}",
               new { controller = "Participante", action = "Index" }
               );

            routes.MapRoute(
              "Home",
              "Home/Index/Index",
              new { controller = "Home", action = "Index" }
              );

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
