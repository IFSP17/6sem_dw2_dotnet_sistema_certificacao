﻿using projeto_v0.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace projeto_v0.Controllers
{
    public class CertificadoDAO
    {
        public bool Create(Certificado certificado)
        {
            try
            {
                System.Diagnostics.Debug.WriteLine("Create part1: " + certificado.cod_assinatura_1);
                DAO.AcessoDadosMySQL.LimparParametros();

                object objRetorno = null;
                if (certificado != null)
                {
                    if (certificado.cod_assinatura_1 == 0)
                    {
                        DAO.AcessoDadosMySQL.AdicionarParametros("@intCod_Assinatura_1", null);
                        DAO.AcessoDadosMySQL.AdicionarParametros("@intCod_Assinatura_2",null);
                        DAO.AcessoDadosMySQL.AdicionarParametros("@intCod_Template", null);
                        DAO.AcessoDadosMySQL.AdicionarParametros("@vchData_emissao",null);
                        DAO.AcessoDadosMySQL.AdicionarParametros("@intCod_participante", certificado.cod_participante);
                        DAO.AcessoDadosMySQL.AdicionarParametros("@intCod_atividade", certificado.cod_atividade);
                        DAO.AcessoDadosMySQL.AdicionarParametros("@intCod_unico_certif", null);
                    }
                    else
                    {
                        DAO.AcessoDadosMySQL.AdicionarParametros("@intCod_Assinatura_1", certificado.cod_assinatura_1);
                        DAO.AcessoDadosMySQL.AdicionarParametros("@intCod_Assinatura_2", certificado.cod_assinatura_2);
                        DAO.AcessoDadosMySQL.AdicionarParametros("@intCod_Template", certificado.cod_template);
                        DAO.AcessoDadosMySQL.AdicionarParametros("@vchData_emissao", certificado.data_emissao);
                        DAO.AcessoDadosMySQL.AdicionarParametros("@intCod_participante", certificado.cod_participante);
                        DAO.AcessoDadosMySQL.AdicionarParametros("@intCod_atividade", certificado.cod_atividade);
                        DAO.AcessoDadosMySQL.AdicionarParametros("@intCod_unico_certif", certificado.cod_unico_certif);
                    }
                    string strSQL = "INSERT INTO certificado (cod_assinatura_1, cod_assinatura_2, cod_template, data_emissao, cod_participante, cod_atividade, cod_unico_certif)" +
                        " VALUES (@intCod_Assinatura_1,@intCod_Assinatura_2,@intCod_Template,STR_TO_DATE(@vchData_emissao,'%d/%m/%Y'), @intCod_participante, @intCod_atividade, @intCod_unico_certif );";
                    objRetorno = DAO.AcessoDadosMySQL.ExecutarManipulacao(System.Data.CommandType.Text, strSQL);
                    System.Diagnostics.Debug.WriteLine("Create part2: " + objRetorno);
                }

                int intResultado = 0;
                if (objRetorno != null)
                {
                    if (int.TryParse(objRetorno.ToString(), out intResultado))
                        return true;
                }

                return false;
            }
            catch (Exception e)
            {
                return false;
            }
        }
        public bool Edit(Certificado certificado)
        {
            try
            {
                DAO.AcessoDadosMySQL.LimparParametros();

                object objRetorno = null;
                if (certificado != null)
                {
                    DAO.AcessoDadosMySQL.AdicionarParametros("@intId", certificado.id);
                    DAO.AcessoDadosMySQL.AdicionarParametros("@intCod_Assinatura_1", certificado.cod_assinatura_1);
                    DAO.AcessoDadosMySQL.AdicionarParametros("@intCod_Assinatura_2", certificado.cod_assinatura_2);
                    DAO.AcessoDadosMySQL.AdicionarParametros("@intCod_Template", certificado.cod_template);
                    DAO.AcessoDadosMySQL.AdicionarParametros("@vchData_emissao", certificado.data_emissao);
                    DAO.AcessoDadosMySQL.AdicionarParametros("@intCod_participante", certificado.cod_participante);
                    DAO.AcessoDadosMySQL.AdicionarParametros("@intCod_atividade", certificado.cod_atividade);
                    DAO.AcessoDadosMySQL.AdicionarParametros("@intCod_unico_certif", certificado.cod_unico_certif);

                    string strSQL = "UPDATE certificado set cod_assinatura_1=@intCod_Assinatura_1, cod_assinatura_2=@intCod_Assinatura_2, cod_template= @intCod_Template," +
                        "data_emissao=STR_TO_DATE(@vchData_emissao,'%d/%m/%Y'), cod_participante= @intCod_participante, cod_atividade=@intCod_atividade, cod_unico_certif=@intCod_unico_certif WHERE id=@intId;";
                    
                    objRetorno = DAO.AcessoDadosMySQL.ExecutarManipulacao(System.Data.CommandType.Text, strSQL);
                }

                int intResultado = 0;
                if (objRetorno != null)
                {
                    if (int.TryParse(objRetorno.ToString(), out intResultado))
                        return true;
                }

                return false;
            }
            catch (Exception)
            {
                return false;
            }
        }
        public bool CompletarCert(Certificado certificado)
        {
            try
            {
                DAO.AcessoDadosMySQL.LimparParametros();

                object objRetorno = null;
                if (certificado != null)
                {
                    DAO.AcessoDadosMySQL.AdicionarParametros("@intId", certificado.id);
                    DAO.AcessoDadosMySQL.AdicionarParametros("@intCod_Assinatura_1", certificado.cod_assinatura_1);
                    DAO.AcessoDadosMySQL.AdicionarParametros("@intCod_Assinatura_2", certificado.cod_assinatura_2);
                    DAO.AcessoDadosMySQL.AdicionarParametros("@intCod_Template", certificado.cod_template);
                    DAO.AcessoDadosMySQL.AdicionarParametros("@vchData_emissao", certificado.data_emissao);
                    DAO.AcessoDadosMySQL.AdicionarParametros("@intCod_participante", certificado.cod_participante);
                    DAO.AcessoDadosMySQL.AdicionarParametros("@intCod_atividade", certificado.cod_atividade);
                    DAO.AcessoDadosMySQL.AdicionarParametros("@intCod_unico_certif", certificado.cod_unico_certif);

                    string strSQL = "UPDATE certificado set cod_assinatura_1=@intCod_Assinatura_1, cod_assinatura_2=@intCod_Assinatura_2, cod_template= @intCod_Template," +
                        "data_emissao=STR_TO_DATE(@vchData_emissao,'%d/%m/%Y'), cod_participante= @intCod_participante, cod_atividade=@intCod_atividade, cod_unico_certif=@intCod_unico_certif WHERE  cod_participante= @intCod_participante AND cod_atividade=@intCod_atividade;";

                    objRetorno = DAO.AcessoDadosMySQL.ExecutarManipulacao(System.Data.CommandType.Text, strSQL);
                }

                int intResultado = 0;
                if (objRetorno != null)
                {
                    if (int.TryParse(objRetorno.ToString(), out intResultado))
                        return true;
                }

                return false;
            }
            catch (Exception)
            {
                return false;
            }
        }
        public bool Delete(Certificado certificado)
        {
            try
            {
                DAO.AcessoDadosMySQL.LimparParametros();

                object objRetorno = null;
                if (certificado != null)
                {
                    DAO.AcessoDadosMySQL.AdicionarParametros("@intId", certificado.id);

                    string strSQL = "DELETE FROM certificado WHERE id = @intId;";
                    objRetorno = DAO.AcessoDadosMySQL.ExecutarManipulacao(System.Data.CommandType.Text, strSQL);
                }

                int intResultado = 0;
                if (objRetorno != null)
                {
                    if (int.TryParse(objRetorno.ToString(), out intResultado))
                        return true;
                }

                return false;
            }
            catch (Exception)
            {
                return false;
            }
        }
        public bool DeleteCertificadoIndeferido(int ativ,int part)
        {
            try
            {
                DAO.AcessoDadosMySQL.LimparParametros();

                object objRetorno = null;
                DAO.AcessoDadosMySQL.AdicionarParametros("@intPart", part);
                DAO.AcessoDadosMySQL.AdicionarParametros("@intAtiv", ativ);

                string strSQL = "DELETE FROM certificado WHERE cod_atividade=@intAtiv AND cod_participante=@intPart;";
                    objRetorno = DAO.AcessoDadosMySQL.ExecutarManipulacao(System.Data.CommandType.Text, strSQL);
                
                int intResultado = 0;
                if (objRetorno != null)
                {
                    if (int.TryParse(objRetorno.ToString(), out intResultado))
                        return true;
                }

                return false;
            }
            catch (Exception)
            {
                return false;
            }
        }
        public List<Certificado> List()
        {
            List<Certificado> lista = new List<Certificado>();
            try
            {
                DataTable objDataTable = null;
                //Se quiser personalizar a busca
                string strSQL = "SELECT id,cod_assinatura_1, cod_assinatura_2,DATE_FORMAT(data_emissao,'%d/%m/%Y') AS data_emissao, cod_template, cod_participante, cod_atividade, cod_unico_certif FROM certificado WHERE data_emissao is not null";
                objDataTable = DAO.AcessoDadosMySQL.ExecutaConsultar(System.Data.CommandType.Text, strSQL);
                if (objDataTable.Rows.Count <= 0)

                {
                    return lista;
                }

                foreach (DataRow objLinha in objDataTable.Rows)
                {
                    Certificado objNovoCertificado = new Certificado();
                    objNovoCertificado.id = objLinha["id"] != DBNull.Value ? Convert.ToInt32(objLinha["id"]) : 0;
                    objNovoCertificado.cod_assinatura_1 = objLinha["cod_assinatura_1"] != DBNull.Value ? Convert.ToInt32(objLinha["cod_assinatura_1"]) : 0;
                    objNovoCertificado.cod_assinatura_2 = objLinha["cod_assinatura_2"] != DBNull.Value ? Convert.ToInt32(objLinha["cod_assinatura_2"]) : 0;
                    objNovoCertificado.data_emissao = objLinha["data_emissao"] != DBNull.Value ? Convert.ToString(objLinha["data_emissao"]) : "";
                    objNovoCertificado.cod_template = objLinha["cod_template"] != DBNull.Value ? Convert.ToInt32(objLinha["cod_template"]) : 0;
                    
                    objNovoCertificado.cod_participante = objLinha["cod_participante"] != DBNull.Value ? Convert.ToInt32(objLinha["cod_participante"]) : 0;
                    objNovoCertificado.cod_atividade = objLinha["cod_atividade"] != DBNull.Value ? Convert.ToInt32(objLinha["cod_atividade"]) : 0;
                    objNovoCertificado.cod_unico_certif = objLinha["cod_unico_certif"] != DBNull.Value ? Convert.ToString(objLinha["cod_unico_certif"]) : "";

                    lista.Add(objNovoCertificado);
                }
                return lista;
            }
            catch (Exception)
            {
                return lista;
            }
        }
        public List<Certificado> List(int id)
        {
            List<Certificado> lista = new List<Certificado>();
            try
            {
                DAO.AcessoDadosMySQL.LimparParametros();
                DAO.AcessoDadosMySQL.AdicionarParametros("@intId", id);
                DataTable objDataTable = null;
                //Se quiser personalizar a busca
                string strSQL = "SELECT id,cod_assinatura_1, cod_assinatura_2,DATE_FORMAT(data_emissao,'%d/%m/%Y') AS data_emissao, cod_template, cod_participante, cod_atividade, cod_unico_certif FROM certificado WHERE id=@intId";
                objDataTable = DAO.AcessoDadosMySQL.ExecutaConsultar(System.Data.CommandType.Text, strSQL);
                if (objDataTable.Rows.Count <= 0)

                {
                    return lista;
                }

                foreach (DataRow objLinha in objDataTable.Rows)
                {
                    Certificado objNovoCertificado = new Certificado();
                    objNovoCertificado.id = objLinha["id"] != DBNull.Value ? Convert.ToInt32(objLinha["id"]) : 0;
                    objNovoCertificado.cod_assinatura_1 = objLinha["cod_assinatura_1"] != DBNull.Value ? Convert.ToInt32(objLinha["cod_assinatura_1"]) : 0;
                    objNovoCertificado.cod_assinatura_2 = objLinha["cod_assinatura_2"] != DBNull.Value ? Convert.ToInt32(objLinha["cod_assinatura_2"]) : 0;
                    objNovoCertificado.data_emissao = objLinha["data_emissao"] != DBNull.Value ? Convert.ToString(objLinha["data_emissao"]) : "";
                    objNovoCertificado.cod_template = objLinha["cod_template"] != DBNull.Value ? Convert.ToInt32(objLinha["cod_template"]) : 0;

                    objNovoCertificado.cod_participante = objLinha["cod_participante"] != DBNull.Value ? Convert.ToInt32(objLinha["cod_participante"]) : 0;
                    objNovoCertificado.cod_atividade = objLinha["cod_atividade"] != DBNull.Value ? Convert.ToInt32(objLinha["cod_atividade"]) : 0;
                    objNovoCertificado.cod_unico_certif = objLinha["cod_unico_certif"] != DBNull.Value ? Convert.ToString(objLinha["cod_unico_certif"]) : "";

                    lista.Add(objNovoCertificado);
                }
                return lista;
            }
            catch (Exception)
            {
                return lista;
            }
        }
        public int BuscaCertificadoExistente(int ativ, int part)
        {
            List<Certificado> lista = new List<Certificado>();
            try
            {
                DAO.AcessoDadosMySQL.LimparParametros();
                DAO.AcessoDadosMySQL.AdicionarParametros("@intAtiv", ativ);
                DAO.AcessoDadosMySQL.AdicionarParametros("@intPart", part);
                DataTable objDataTable = null;
                //Se quiser personalizar a busca
                string strSQL = "SELECT id,cod_assinatura_1, cod_assinatura_2,DATE_FORMAT(data_emissao,'%d/%m/%Y') "+
                    "AS data_emissao, cod_template, cod_participante, cod_atividade, cod_unico_certif FROM certificado "+
                    "WHERE cod_participante=@intPart AND cod_atividade=@intAtiv";
                objDataTable = DAO.AcessoDadosMySQL.ExecutaConsultar(System.Data.CommandType.Text, strSQL);
                if (objDataTable.Rows.Count <= 0)

                {
                    return 0;
                }

                foreach (DataRow objLinha in objDataTable.Rows)
                {
                    Certificado objNovoCertificado = new Certificado();
                    objNovoCertificado.id = objLinha["id"] != DBNull.Value ? Convert.ToInt32(objLinha["id"]) : 0;
                    objNovoCertificado.cod_assinatura_1 = objLinha["cod_assinatura_1"] != DBNull.Value ? Convert.ToInt32(objLinha["cod_assinatura_1"]) : 0;
                    objNovoCertificado.cod_assinatura_2 = objLinha["cod_assinatura_2"] != DBNull.Value ? Convert.ToInt32(objLinha["cod_assinatura_2"]) : 0;
                    objNovoCertificado.data_emissao = objLinha["data_emissao"] != DBNull.Value ? Convert.ToString(objLinha["data_emissao"]) : "";
                    objNovoCertificado.cod_template = objLinha["cod_template"] != DBNull.Value ? Convert.ToInt32(objLinha["cod_template"]) : 0;
                    objNovoCertificado.cod_participante = objLinha["cod_participante"] != DBNull.Value ? Convert.ToInt32(objLinha["cod_participante"]) : 0;
                    objNovoCertificado.cod_atividade = objLinha["cod_atividade"] != DBNull.Value ? Convert.ToInt32(objLinha["cod_atividade"]) : 0;
                    objNovoCertificado.cod_unico_certif = objLinha["cod_unico_certif"] != DBNull.Value ? Convert.ToString(objLinha["cod_unico_certif"]) : "";

                    lista.Add(objNovoCertificado);
                }
                return lista.Count;
            }
            catch (Exception)
            {
                return 0;
            }
        }
        public List<CertificadoParticipante> ListCertificadoPDF(int id)
        {
            List<CertificadoParticipante> lista = new List<CertificadoParticipante>();
            try
            {
                DAO.AcessoDadosMySQL.LimparParametros();
                DAO.AcessoDadosMySQL.AdicionarParametros("@intId", id);
                DataTable objDataTable = null;
                DataTable objDataTable1 = null;
                //Se quiser personalizar a busca
                string strSQL = "SELECT p.nome,p.cpf,c.cod_unico_certif,a.resumo,a.nome AS atividade,DATE_FORMAT(c.data_emissao,'%d/%m/%Y') AS data_emissao,"+
                                "a.localizacao,a.carga_horaria,DATE_FORMAT(a.data_ini,'%d/%m/%Y') AS data_ini,DATE_FORMAT(a.data_fim,'%d/%m/%Y') AS data_fim " +
                                "FROM participante p, certificado c, atividade a WHERE p.id = c.cod_participante AND " +
                                "a.id = c.cod_atividade AND c.id = @intId;";
                objDataTable = DAO.AcessoDadosMySQL.ExecutaConsultar(System.Data.CommandType.Text, strSQL);

                var strSQL1 = "SELECT ass.nome, ass.cargo FROM assinatura ass,certificado c WHERE " +
                                "(ass.id=c.cod_assinatura_1 OR ass.id=c.cod_assinatura_2) AND c.id=@intId;";
                objDataTable1 = DAO.AcessoDadosMySQL.ExecutaConsultar(System.Data.CommandType.Text, strSQL1);

                if (objDataTable.Rows.Count <= 0)

                {
                    return lista;
                }

                foreach (DataRow objLinha in objDataTable.Rows)
                {
                    CertificadoParticipante objNovoCertificado = new CertificadoParticipante();
                    objNovoCertificado.nome_participante = objLinha["nome"] != DBNull.Value ? Convert.ToString(objLinha["nome"]) : "";
                    objNovoCertificado.resumo = objLinha["resumo"] != DBNull.Value ? Convert.ToString(objLinha["resumo"]) : "";
                    objNovoCertificado.cpf_participante = objLinha["cpf"] != DBNull.Value ? Convert.ToString(objLinha["cpf"]) : "";
                    objNovoCertificado.cod_unico_certif = objLinha["cod_unico_certif"] != DBNull.Value ? Convert.ToString(objLinha["cod_unico_certif"]) : "";
                    objNovoCertificado.nome_atividade = objLinha["atividade"] != DBNull.Value ? Convert.ToString(objLinha["atividade"]) : "";
                    objNovoCertificado.data_emissao = objLinha["data_emissao"] != DBNull.Value ? Convert.ToString(objLinha["data_emissao"]) : "";
                    objNovoCertificado.local = objLinha["localizacao"] != DBNull.Value ? Convert.ToString(objLinha["localizacao"]) : "";
                    objNovoCertificado.carga_horaria = objLinha["carga_horaria"] != DBNull.Value ? Convert.ToString(objLinha["carga_horaria"]) : "";
                    objNovoCertificado.data_inicio = objLinha["data_ini"] != DBNull.Value ? Convert.ToString(objLinha["data_ini"]) : "";
                    objNovoCertificado.data_fim = objLinha["data_fim"] != DBNull.Value ? Convert.ToString(objLinha["data_fim"]) : "";

                    if (objDataTable1.Rows.Count > 0)
                    {
                        objNovoCertificado.listAssinaturas = new List<Assinatura>();
                        foreach (DataRow objLinha1 in objDataTable1.Rows)
                        {
                            Assinatura assinatura = new Assinatura();
                            assinatura.nome = objLinha1["nome"] != DBNull.Value ? Convert.ToString(objLinha1["nome"]) : "";
                            assinatura.cargo = objLinha1["cargo"] != DBNull.Value ? Convert.ToString(objLinha1["cargo"]) : "";
                            objNovoCertificado.listAssinaturas.Add(assinatura);
                            
                        }
                    }
                        lista.Add(objNovoCertificado);
                }
                return lista;
            }
            catch (Exception e)
            {

                System.Diagnostics.Debug.WriteLine("Ex: " + e.ToString());
                return lista;
            }
        }
    }
}