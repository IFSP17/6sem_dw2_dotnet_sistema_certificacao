﻿using projeto_v0.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using Excel;
using System.Data;

namespace projeto_v0.Controllers
{
    public class ParticipanteController : Controller
    {
        ParticipanteDAO participanteDAO = new ParticipanteDAO();
        public static List<Participante> listaParticipante = new List<Participante>();

        // GET: Participante
        public ActionResult Index(int? id)
        {
            System.Diagnostics.Debug.WriteLine("This will be displayed in output window" + id);
            listaParticipante = participanteDAO.List();
            return View(listaParticipante);
            
        }

    // GET: Participante/Details/5
    public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Participante/Create
        public ActionResult Create()
        {
            return View();
        }
        
        // POST: Participante/Create
        [HttpPost]
        public ActionResult Create(Participante participante)
        {
            try
            {
                string cpfEncontrado = participanteDAO.BuscaCPF(participante.cpf);
                if (cpfEncontrado == "")
                {
                    listaParticipante.Add(participante);
                    participanteDAO.Create(participante);

                    //return RedirectToAction("Index");
                    return RedirectToAction("Index").Mensagem("Matricula registrada com sucesso!", "Sucesso");

                }
                else
                {
                    //RETORNAR MENSAGEM DE CPF JÁ CADASTRADO
                    //return View();
                    return View().Mensagem("CPF já cadastrado!", "Atenção");
                }
            }
            catch
            {
                //return View();
                return View().Mensagem("Erro ao registrar matricula!", "Erro");
            }
        }

        // GET: Participante/Edit/5
        public ActionResult Edit(int id)
        {
            var participante = listaParticipante.Single(p => p.id == id);

            return View(participante);
        }

        // POST: Participante/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                var participante = listaParticipante.Single(p => p.id == id);
                if (TryUpdateModel(participante))
                {
                    participanteDAO.Edit(participante);
                    //return RedirectToAction("Index");
                    return RedirectToAction("Index").Mensagem("Matricula alterada com sucesso!", "Sucesso");

                }
                return View(participante);
            }
            catch
            {
                //return View();
                return View().Mensagem("Erro ao alterar matricula!", "Erro");

            }
        }

        // GET: Participante/Delete/5
        public ActionResult Delete(int id)
        {
            var participante = listaParticipante.Single(p => p.id == id);
            return View(participante);
        }

        // POST: Participante/Delete/5
        [HttpPost]
        // public ActionResult Delete(int id, FormCollection collection)
        public ActionResult Delete(Participante participante)
        {
            try
            {
                listaParticipante.Remove(participante);
                participanteDAO.Delete(participante);
                //return RedirectToAction("Index");
                return RedirectToAction("Index").Mensagem("Matricula excluída com sucesso!", "Sucesso");

            }
            catch
            {
                //return View();
                return View().Mensagem("Erro ao excluir matricula!", "Erro");
            }
        }
    }
}
