﻿using projeto_v0.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;


namespace projeto_v0.Controllers
{
    public class AtividadeController : Controller    {

        AtividadeDAO atividadeDAO = new AtividadeDAO();
        public static List<Atividade> listaAtividade = new List<Atividade>();
        // GET: Atividade
        public ActionResult Index()
        {
            listaAtividade = atividadeDAO.List();
            return View(listaAtividade);
            
        }

        // GET: Atividade/Create
        public ActionResult Create()
        {
            return View();            
        }
        // POST: Atividade/Create
        [HttpPost]
        public ActionResult Create(Atividade atividade)
        {
            try
            {

                listaAtividade.Add(atividade);
                atividadeDAO.Create(atividade);
                //return RedirectToAction("Index");
                if (atividade == null) throw new Exception();
                return RedirectToAction("Index").Mensagem("Atividade registrada com sucesso!", "Sucesso");

            }
            catch
            {
                //return View();                
                return View().Mensagem("Erro ao registrar Atividade!", "Erro");

            }
        }

        // GET: Atividade/Edit/5
        public ActionResult Edit(int id)
        {
            var atividade = listaAtividade.Single(p => p.id == id);

            return View(atividade);
        }

        // POST: Atividade/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                var atividade = listaAtividade.Single(p => p.id == id);
                if (TryUpdateModel(atividade))
                {
                    atividadeDAO.Edit(atividade);
                    return RedirectToAction("Index");

                }
                //return View(atividade);
                return View().Mensagem("Atividade alterada com sucesso!", "Sucesso");

            }
            catch
            {
                //return View();
                return View().Mensagem("Erro ao alterar atividade!", "Erro");
            }
        }

        // GET: Atividade/Delete/5
        public ActionResult Delete(int id)
        {
            Console.WriteLine("Usuario: " + id);
            var atividade = listaAtividade.Single(p => p.id == id);
            return View(atividade);
        }

        // POST: Atividade/Delete/5
        [HttpPost]
        //        public ActionResult Delete(int id, FormCollection collection)
        public ActionResult Delete(Atividade atividade)
        {
            try
            {
                listaAtividade.Remove(atividade);
                atividadeDAO.Delete(atividade);
                //return RedirectToAction("Index");
                return RedirectToAction("Index").Mensagem("Atividade excluído com sucesso!", "Sucesso");

            }
            catch
            {
                //return View();
                return View().Mensagem("Erro ao excluir atividade!", "Erro");
            }
        }
        public ActionResult Detalhes(int id)
        {
            ViewBag.id=id;
            return View();
        }
    }
}

