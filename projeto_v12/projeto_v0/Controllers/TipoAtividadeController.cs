﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using projeto_v0.Models;

namespace projeto_v0.Controllers
{
    public class TipoAtividadeController : Controller
    {
        TipoAtividadeDAO TipoAtividadeDAO = new TipoAtividadeDAO();
        public static List<TipoAtividade> listaTipoAtividade = new List<TipoAtividade>();
        // GET: TipoAtividade
        public ActionResult Index()
        {
            //USARÁ???
            listaTipoAtividade = TipoAtividadeDAO.List();
            return View(listaTipoAtividade);
            //var TipoAtividades = from b in listaTipoAtividade
                             // orderby b.id
                                 //select b;
           // return View(TipoAtividades);
        }

        // GET: TipoAtividade/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: TipoAtividade/Create
        public ActionResult Create()
        {
            return View();
        }
        //DECIDIR QUAL UTILIZAR
        /*
        // POST: Banda/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                TipoAtividade TipoAtividade = new TipoAtividade();
                TipoAtividade.cod_TipoAtividade = listaTipoAtividade.Count;
                TipoAtividade.nome = collection["nome"];
                TipoAtividade.caminho = collection["caminho"];
                TipoAtividade.cargo = collection["cargo"];

                listaTipoAtividade.Add(TipoAtividade);

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
        */

        // POST: TipoAtividade/Create
        [HttpPost]
        public ActionResult Create(TipoAtividade TipoAtividade)
        {
            try
            {
                // TODO: Add insert logic here

                listaTipoAtividade.Add(TipoAtividade);
                TipoAtividadeDAO.Create(TipoAtividade);
                //return RedirectToAction("Index");
                return RedirectToAction("Index").Mensagem("Tipo de Atividade registrado com sucesso!", "Sucesso");

            }
            catch
            {
                //return View();
                return View().Mensagem("Erro ao registrar Tipo de Atividade!", "Erro");

            }
        }

        // GET: TipoAtividade/Edit/5
        public ActionResult Edit(int id)
        {

            var TipoAtividade = listaTipoAtividade.Single(p => p.id == id);

            return View(TipoAtividade);
        }

        // POST: TipoAtividade/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                var TipoAtividade = listaTipoAtividade.Single(p => p.id == id);
                if (TryUpdateModel(TipoAtividade))
                {
                    TipoAtividadeDAO.Edit(TipoAtividade);
                    //return RedirectToAction("Index");
                    return RedirectToAction("Index").Mensagem("Tipo de Atividade alterado com sucesso!", "Sucesso");

                }
                return View(TipoAtividade);

            }
            catch
            {
                //return View();
                return View().Mensagem("Erro ao alterar tipo de Atividade!", "Erro");

            }
        }

        // GET: TipoAtividade/Delete/5
        public ActionResult Delete(int id)
        {
            var TipoAtividade = listaTipoAtividade.Single(p => p.id == id);
            return View(TipoAtividade);
        }

        // POST: TipoAtividade/Delete/5
        [HttpPost]
        public ActionResult Delete(TipoAtividade TipoAtividade)
        {
            try
            {
                listaTipoAtividade.Remove(TipoAtividade);
                TipoAtividadeDAO.Delete(TipoAtividade);
                //return RedirectToAction("Index");
                return RedirectToAction("Index").Mensagem("Tipo de Atividade excluído com sucesso!", "Sucesso");

            }
            catch
            {
                //return View();
                return View().Mensagem("Erro ao excluir Tipo de Atividade!", "Erro");

            }
        }
        /*
        public static List<TipoAtividade> listaTipoAtividade = new List<TipoAtividade>{
           new TipoAtividade{
              cod_TipoAtividade = 1,
              nome = "Anderson",
              caminho="/c",
              cargo="coodenador"
           },
            new TipoAtividade{
              cod_TipoAtividade = 2,
              nome = "Anderson",
              caminho="/c",
              cargo="diretor",
           },           
        };*/
    }
}
