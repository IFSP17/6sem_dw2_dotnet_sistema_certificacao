﻿using projeto_v0.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace projeto_v0.Controllers
{  
    public class UsuarioController : Controller
    {

        UsuarioDAO usuarioDAO = new UsuarioDAO();
        public static List<Usuario> listaUsuario = new List<Usuario>();       

        // GET: Usuario
        public ActionResult Index()
        {
            listaUsuario = usuarioDAO.List();
            return View(listaUsuario);
            
        }

        // GET: Usuario/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Usuario/Create
        public ActionResult Create()
        {
            return View();
        }
        //DECIDIR QUAL UTILIZAR
        /*
        // POST: Banda/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                Usuario usuario = new Usuario();
                usuario.id = listaUsuario.Count;
                usuario.nome = collection["nome"];
                usuario.usuario = collection["usuario"];
                usuario.email = collection["email"];
                usuario.senha = collection["senha"];
                
                listaUsuario.Add(usuario);

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
        
    */
        // POST: Usuario/Create
        [HttpPost]
        public ActionResult Create(Usuario usuario)
        {
            try
            {
                listaUsuario.Add(usuario);                
                usuarioDAO.Create(usuario);

                //return RedirectToAction("Index");
                return RedirectToAction("Index").Mensagem("Usuário registrado com sucesso!", "Sucesso");

            }
            catch
            {
                //return View();
                return View().Mensagem("Erro ao registrar usuário!", "Erro");
            }
        }

        // GET: Usuario/Edit/5
        public ActionResult Edit(int id)
        {
            var usuario = listaUsuario.Single(p => p.id == id);

            return View(usuario);
        }

        // POST: Usuario/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                var usuario = listaUsuario.Single(p => p.id == id);
                if (TryUpdateModel(usuario))
                {
                    usuarioDAO.Edit(usuario);
                    //return RedirectToAction("Index");
                    return RedirectToAction("Index").Mensagem("Usuário alterado com sucesso!", "Sucesso");
                }
                return View(usuario);
            }
            catch
            {
                //return View();
                return View().Mensagem("Erro ao alterar usuário!", "Erro");
            }
        }

        // GET: Usuario/Delete/5
        public ActionResult Delete(int id)
        {
            var usuario = listaUsuario.Single(p => p.id == id);
            return View(usuario);
        }

        // POST: Usuario/Delete/5
        [HttpPost]
        // public ActionResult Delete(int id, FormCollection collection)
        public ActionResult Delete(Usuario usuario)
        {
            try
            {
                listaUsuario.Remove(usuario);
                usuarioDAO.Delete(usuario);
                //return RedirectToAction("Index");
                return RedirectToAction("Index").Mensagem("Usuário excluído com sucesso!", "Sucesso");

            }
            catch
            {
                //return View();
                return View().Mensagem("Erro ao excluir usuário!", "Erro");
            }
        }
    }
}
