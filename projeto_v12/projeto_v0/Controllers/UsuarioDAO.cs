﻿using projeto_v0.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace projeto_v0.Controllers
{
    public class UsuarioDAO
    {
        public bool Create(Usuario usuario)
        {
            try
            {
                DAO.AcessoDadosMySQL.LimparParametros();

                object objRetorno = null;
                if (usuario != null)
                {
                    
                    DAO.AcessoDadosMySQL.AdicionarParametros("@intId", usuario.id);
                    DAO.AcessoDadosMySQL.AdicionarParametros("@vchNome", usuario.nome);
                    DAO.AcessoDadosMySQL.AdicionarParametros("@vchEmail", usuario.email);
                    DAO.AcessoDadosMySQL.AdicionarParametros("@vchSenha", usuario.senha);
                    DAO.AcessoDadosMySQL.AdicionarParametros("@vchUser", usuario.user);

                    string strSQL = "INSERT INTO usuario (id,nome,email,senha,user)" +
                        " VALUES (@intId,@vchNome,@vchEmail,@vchSenha,@vchUser);";
                    objRetorno = DAO.AcessoDadosMySQL.ExecutarManipulacao(System.Data.CommandType.Text, strSQL);
                }

                int intResultado = 0;
                if (objRetorno != null)
                {
                    if (int.TryParse(objRetorno.ToString(), out intResultado))
                        return true;
                }

                return false;
            }
            catch (Exception)
            {
                return false;
            }
        }
        public bool Edit(Usuario usuario)
        {
            try
            {
                DAO.AcessoDadosMySQL.LimparParametros();

                object objRetorno = null;
                if (usuario != null)
                {
                    Console.WriteLine("Ola");
                    DAO.AcessoDadosMySQL.AdicionarParametros("@intId", usuario.id);
                    DAO.AcessoDadosMySQL.AdicionarParametros("@vchNome", usuario.nome);
                    DAO.AcessoDadosMySQL.AdicionarParametros("@vchEmail", usuario.email);
                    DAO.AcessoDadosMySQL.AdicionarParametros("@vchSenha", usuario.senha);
                    DAO.AcessoDadosMySQL.AdicionarParametros("@vchUser", usuario.user);

                    string strSQL = "UPDATE usuario set nome=@vchNome,email=@vchEmail,senha=@vchSenha,user=@vchUser"
                        + " WHERE id = @intId;";
                    objRetorno = DAO.AcessoDadosMySQL.ExecutarManipulacao(System.Data.CommandType.Text, strSQL);
                }

                int intResultado = 0;
                if (objRetorno != null)
                {
                    if (int.TryParse(objRetorno.ToString(), out intResultado))
                        return true;
                }

                return false;
            }
            catch (Exception)
            {
                return false;
            }
        }
        public bool Delete(Usuario usuario)
        {
            try
            {
                DAO.AcessoDadosMySQL.LimparParametros();

                object objRetorno = null;
                if (usuario != null)
                {
                    DAO.AcessoDadosMySQL.AdicionarParametros("@intId", usuario.id);

                    string strSQL = "DELETE FROM usuario WHERE id= @intId;";
                    objRetorno = DAO.AcessoDadosMySQL.ExecutarManipulacao(System.Data.CommandType.Text, strSQL);
                }

                int intResultado = 0;
                if (objRetorno != null)
                {
                    if (int.TryParse(objRetorno.ToString(), out intResultado))
                        return true;
                }

                return false;
            }
            catch (Exception)
            {
                return false;
            }
        }
        public List<Usuario> List()
        {
            List<Usuario> lista = new List<Usuario>();
            try
            {
                DataTable objDataTable = null;
                //Se quiser personalizar a busca
                string strSQL = "SELECT id,nome,email,senha,user FROM usuario";
                objDataTable = DAO.AcessoDadosMySQL.ExecutaConsultar(System.Data.CommandType.Text, strSQL);
                if (objDataTable.Rows.Count <= 0)

                {
                    return lista;
                }

                foreach (DataRow objLinha in objDataTable.Rows)
                {
                    Usuario objNovoUsuario = new Usuario();
                    objNovoUsuario.id = objLinha["id"] != DBNull.Value ? Convert.ToInt32(objLinha["id"]) : 0;
                    objNovoUsuario.nome = objLinha["nome"] != DBNull.Value ? Convert.ToString(objLinha["nome"]) : "";
                    objNovoUsuario.senha = objLinha["senha"] != DBNull.Value ? Convert.ToString(objLinha["senha"]) : "";
                    objNovoUsuario.user = objLinha["user"] != DBNull.Value ? Convert.ToString(objLinha["user"]) : "";
                    objNovoUsuario.email = objLinha["email"] != DBNull.Value ? Convert.ToString(objLinha["email"]) : "";

                    lista.Add(objNovoUsuario);
                }
                return lista;
            }
            catch (Exception)
            {
                return lista;
            }
        }
    }
}