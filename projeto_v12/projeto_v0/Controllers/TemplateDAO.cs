﻿using projeto_v0.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace projeto_v0.Controllers
{
    public class TemplateDAO
    {
        public bool Create(Template template)
        {
            try
            {
                DAO.AcessoDadosMySQL.LimparParametros();

                object objRetorno = null;
                if (template != null)
                {
                    DAO.AcessoDadosMySQL.AdicionarParametros("@intId", template.id);
                    DAO.AcessoDadosMySQL.AdicionarParametros("@vchCaminho", template.caminho);
                    DAO.AcessoDadosMySQL.AdicionarParametros("@vchDesc", template.desc);                   

                    string strSQL = "INSERT INTO template (id,caminho,descr)" +
                        " VALUES (@intId,@vchCaminho,@vchDesc);";
                    objRetorno = DAO.AcessoDadosMySQL.ExecutarManipulacao(System.Data.CommandType.Text, strSQL);
                }

                int intResultado = 0;
                if (objRetorno != null)
                {
                    if (int.TryParse(objRetorno.ToString(), out intResultado))
                        return true;
                }

                return false;
            }
            catch (Exception)
            {
                return false;
            }
        }
        public bool Edit(Template template)
        {
            try
            {
                DAO.AcessoDadosMySQL.LimparParametros();

                object objRetorno = null;
                if (template != null)
                {
                    DAO.AcessoDadosMySQL.AdicionarParametros("@intId", template.id);
                    DAO.AcessoDadosMySQL.AdicionarParametros("@vchCaminho", template.caminho);
                    DAO.AcessoDadosMySQL.AdicionarParametros("@vchDesc", template.desc);

                    string strSQL = "UPDATE template set caminho=@vchCaminho, descr=@vchDesc"
                        + " WHERE id = @intId;";
                    objRetorno = DAO.AcessoDadosMySQL.ExecutarManipulacao(System.Data.CommandType.Text, strSQL);
                }

                int intResultado = 0;
                if (objRetorno != null)
                {
                    if (int.TryParse(objRetorno.ToString(), out intResultado))
                        return true;
                }

                return false;
            }
            catch (Exception)
            {
                return false;
            }
        }
        public bool Delete(Template template)
        {
            try
            {
                DAO.AcessoDadosMySQL.LimparParametros();

                object objRetorno = null;
                if (template != null)
                {
                    DAO.AcessoDadosMySQL.AdicionarParametros("@intId", template.id);

                    string strSQL = "DELETE FROM template WHERE id = @intId; select @intId;";
                    objRetorno = DAO.AcessoDadosMySQL.ExecutarManipulacao(System.Data.CommandType.Text, strSQL);
                }

                int intResultado = 0;
                if (objRetorno != null)
                {
                    if (int.TryParse(objRetorno.ToString(), out intResultado))
                        return true;
                }

                return false;
            }
            catch (Exception)
            {
                return false;
            }
        }
        public List<Template> List()
        {
            List<Template> lista = new List<Template>();
            try
            {
                DataTable objDataTable = null;
                //Se quiser personalizar a busca
                string strSQL = "SELECT id,caminho,descr FROM template";
                objDataTable = DAO.AcessoDadosMySQL.ExecutaConsultar(System.Data.CommandType.Text, strSQL);
                if (objDataTable.Rows.Count <= 0)

                {
                    return lista;
                }

                foreach (DataRow objLinha in objDataTable.Rows)
                {
                    Template objNovoTemplate = new Template();
                    objNovoTemplate.id = objLinha["id"] != DBNull.Value ? Convert.ToInt32(objLinha["id"]) : 0;
                    objNovoTemplate.caminho = objLinha["caminho"] != DBNull.Value ? Convert.ToString(objLinha["caminho"]) : "";                  
                    objNovoTemplate.desc = objLinha["descr"] != DBNull.Value ? Convert.ToString(objLinha["descr"]) : "";

                    lista.Add(objNovoTemplate);
                }
                return lista;
            }
            catch (Exception)
            {
                return lista;
            }
        }
    }
}