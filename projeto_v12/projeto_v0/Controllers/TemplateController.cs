﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using projeto_v0.Models;

namespace projeto_v0.Controllers
{
    public class TemplateController : Controller
    {
        TemplateDAO TemplateDAO = new TemplateDAO();
        public static List<Template> listaTemplate = new List<Template>();
        // GET: Template
        public ActionResult Index()
        {
            //USARÁ???
            listaTemplate = TemplateDAO.List();
            return View(listaTemplate);
            //var Templates = from b in listaTemplate
                            //orderby b.id
                            //select b;
            //return View(Templates);
        }

        // GET: Template/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Template/Create
        public ActionResult Create()
        {
            return View();
        }
        //DECIDIR QUAL UTILIZAR
        /*
        // POST: Banda/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                Template Template = new Template();
                Template.id = listaTemplate.Count;
                Template.nome = collection["nome"];
                Template.caminho = collection["caminho"];
                Template.cargo = collection["cargo"];

                listaTemplate.Add(Template);

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
        */

        // POST: Template/Create
        [HttpPost]
        public ActionResult Create(Template Template)
        {
            try
            {
                // TODO: Add insert logic here

                listaTemplate.Add(Template);
                TemplateDAO.Create(Template);
                //return RedirectToAction("Index");
                return RedirectToAction("Index").Mensagem("Template registrado com sucesso!", "Sucesso");

            }
            catch
            {
                //return View();
                return View().Mensagem("Erro ao registrar template!", "Erro");
            }
        }

        // GET: Template/Edit/5
        public ActionResult Edit(int id)
        {

            var Template = listaTemplate.Single(p => p.id == id);

            return View(Template);
        }

        // POST: Template/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                var Template = listaTemplate.Single(p => p.id == id);
                if (TryUpdateModel(Template))
                {
                    TemplateDAO.Edit(Template);
                    //return RedirectToAction("Index");
                    return RedirectToAction("Index").Mensagem("Template alterado com sucesso!", "Sucesso");

                }
                return View(Template);

            }
            catch
            {
                //return View();
                return View().Mensagem("Erro ao alterar template!", "Erro");

            }
        }

        // GET: Template/Delete/5
        public ActionResult Delete(int id)
        {
            var Template = listaTemplate.Single(p => p.id == id);
            return View(Template);
        }

        // POST: Template/Delete/5
        [HttpPost]
        public ActionResult Delete(Template Template)
        {
            try
            {
                listaTemplate.Remove(Template);
                TemplateDAO.Delete(Template);
                //return RedirectToAction("Index");
                return RedirectToAction("Index").Mensagem("Template excluído com sucesso!", "Sucesso");

            }
            catch
            {
                //return View();
                return View().Mensagem("Erro ao excluir template!", "Erro");

            }
        }
        /*
        public static List<Template> listaTemplate = new List<Template>{
           new Template{
              id = 1,
              nome = "Anderson",
              caminho="/c",
              cargo="coodenador"
           },
            new Template{
              id = 2,
              nome = "Anderson",
              caminho="/c",
              cargo="diretor",
           },           
        };*/
    }
}
