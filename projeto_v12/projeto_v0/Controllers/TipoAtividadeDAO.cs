﻿using projeto_v0.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace projeto_v0.Controllers
{
    public class TipoAtividadeDAO
    {
        public bool Create(TipoAtividade tipoAtividade)
        {
            try
            {
                DAO.AcessoDadosMySQL.LimparParametros();

                object objRetorno = null;
                if (tipoAtividade != null)
                {
                    DAO.AcessoDadosMySQL.AdicionarParametros("@intId", tipoAtividade.id);                  
                    DAO.AcessoDadosMySQL.AdicionarParametros("@intCod_template", tipoAtividade.cod_template);
                    DAO.AcessoDadosMySQL.AdicionarParametros("@vchDesc", tipoAtividade.desc);

                    string strSQL = "INSERT INTO tipoatividade (id,cod_template,descr)" +
                        " VALUES (@intId,@intCod_template,@vchDesc);";
                    objRetorno = DAO.AcessoDadosMySQL.ExecutarManipulacao(System.Data.CommandType.Text, strSQL);
                }

                int intResultado = 0;
                if (objRetorno != null)
                {
                    if (int.TryParse(objRetorno.ToString(), out intResultado))
                        return true;
                }

                return false;
            }
            catch (Exception)
            {
                return false;
            }
        }
        public bool Edit(TipoAtividade tipoAtividade)
        {
            try
            {
                DAO.AcessoDadosMySQL.LimparParametros();

                object objRetorno = null;
                if (tipoAtividade != null)
                {
                    DAO.AcessoDadosMySQL.AdicionarParametros("@intId", tipoAtividade.id);                   
                    DAO.AcessoDadosMySQL.AdicionarParametros("@intCod_template", tipoAtividade.cod_template);
                    DAO.AcessoDadosMySQL.AdicionarParametros("@vchDesc", tipoAtividade.desc);

                    string strSQL = "UPDATE tipoatividade set cod_template=@intCod_template,descr=@vchDesc"
                        + " WHERE id = @intId;";
                    objRetorno = DAO.AcessoDadosMySQL.ExecutarManipulacao(System.Data.CommandType.Text, strSQL);
                }

                int intResultado = 0;
                if (objRetorno != null)
                {
                    if (int.TryParse(objRetorno.ToString(), out intResultado))
                        return true;
                }

                return false;
            }
            catch (Exception)
            {
                return false;
            }
        }
        public bool Delete(TipoAtividade tipoAtividade)
        {
            try
            {
                DAO.AcessoDadosMySQL.LimparParametros();

                object objRetorno = null;
                if (tipoAtividade != null)
                {
                    DAO.AcessoDadosMySQL.AdicionarParametros("@intId", tipoAtividade.id);

                    string strSQL = "DELETE FROM tipoatividade WHERE id = @intId;";
                    objRetorno = DAO.AcessoDadosMySQL.ExecutarManipulacao(System.Data.CommandType.Text, strSQL);
                }

                int intResultado = 0;
                if (objRetorno != null)
                {
                    if (int.TryParse(objRetorno.ToString(), out intResultado))
                        return true;
                }

                return false;
            }
            catch (Exception)
            {
                return false;
            }
        }
        public List<TipoAtividade> List()
        {
            List<TipoAtividade> lista = new List<TipoAtividade>();
            try
            {
                DataTable objDataTable = null;
                //Se quiser personalizar a busca
                string strSQL = "SELECT id,cod_template,descr FROM tipoatividade";
                objDataTable = DAO.AcessoDadosMySQL.ExecutaConsultar(System.Data.CommandType.Text, strSQL);
                if (objDataTable.Rows.Count <= 0)

                {
                    return lista;
                }

                foreach (DataRow objLinha in objDataTable.Rows)
                {
                    TipoAtividade objNovoTipoAtividade = new TipoAtividade();
                    objNovoTipoAtividade.id = objLinha["id"] != DBNull.Value ? Convert.ToInt32(objLinha["id"]) : 0;                  
                    objNovoTipoAtividade.cod_template = objLinha["cod_template"] != DBNull.Value ? Convert.ToInt32(objLinha["cod_template"]) : 0;
                    objNovoTipoAtividade.desc = objLinha["descr"] != DBNull.Value ? Convert.ToString(objLinha["descr"]) : "";

                    lista.Add(objNovoTipoAtividade);
                }
                return lista;
            }
            catch (Exception)
            {
                return lista;
            }
        }
    }
}