﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using projeto_v0.Models;
using System.Data;

namespace projeto_v0.Controllers
{
    public class AtividadeDAO
    {
        public bool Create(Atividade atividade)
        {
            try
            {
                DAO.AcessoDadosMySQL.LimparParametros();

                object objRetorno = null;
                if (atividade != null)
                {
                    DAO.AcessoDadosMySQL.AdicionarParametros("@intId", atividade.id);
                    DAO.AcessoDadosMySQL.AdicionarParametros("@vchNome", atividade.nome);
                    DAO.AcessoDadosMySQL.AdicionarParametros("@vchResumo", atividade.resumo);
                    DAO.AcessoDadosMySQL.AdicionarParametros("@vchData_ini", atividade.data_ini);
                    DAO.AcessoDadosMySQL.AdicionarParametros("@vchData_fim", atividade.data_fim);
                    DAO.AcessoDadosMySQL.AdicionarParametros("@vchCarga_horaria", atividade.carga_horaria);
                    DAO.AcessoDadosMySQL.AdicionarParametros("@vchAno", atividade.ano);
                    DAO.AcessoDadosMySQL.AdicionarParametros("@intCod_responsavel", atividade.cod_responsavel);
                    DAO.AcessoDadosMySQL.AdicionarParametros("@vchLocal", atividade.local);
                    DAO.AcessoDadosMySQL.AdicionarParametros("@intCod_tipo_ativ", atividade.cod_tipo_ativ);

                    string strSQL = "INSERT INTO atividade (id,nome,resumo,data_ini,data_fim,carga_horaria,ano,cod_responsavel," +
                        "localizacao,cod_tipo_ativ) values (@intId,@vchNome,@vchResumo,STR_TO_DATE(@vchData_ini,'%d/%m/%Y'),STR_TO_DATE(@vchData_fim,'%d/%m/%Y'),@vchCarga_horaria,@vchAno,@intCod_responsavel," +
                        "@vchLocal,@intCod_tipo_ativ);";
                    objRetorno = DAO.AcessoDadosMySQL.ExecutarManipulacao(System.Data.CommandType.Text, strSQL);
                }

                int intResultado = 0;
                if (objRetorno != null)
                {
                    if (int.TryParse(objRetorno.ToString(), out intResultado))
                        return true;                  

                }

                return false;
            }
            catch (Exception)
            {
                return false;
            }
        }
        public bool Edit(Atividade atividade)
        {
            try
            {
                DAO.AcessoDadosMySQL.LimparParametros();

                object objRetorno = null;
                if (atividade != null)
                {
                    DAO.AcessoDadosMySQL.AdicionarParametros("@intId", atividade.id);
                    DAO.AcessoDadosMySQL.AdicionarParametros("@vchNome", atividade.nome);
                    DAO.AcessoDadosMySQL.AdicionarParametros("@vchResumo", atividade.resumo);
                    DAO.AcessoDadosMySQL.AdicionarParametros("@vchData_ini", atividade.data_ini);
                    DAO.AcessoDadosMySQL.AdicionarParametros("@vchData_fim", atividade.data_fim);
                    DAO.AcessoDadosMySQL.AdicionarParametros("@vchCarga_horaria", atividade.carga_horaria);
                    DAO.AcessoDadosMySQL.AdicionarParametros("@vchAno", atividade.ano);
                    DAO.AcessoDadosMySQL.AdicionarParametros("@intCod_responsavel", atividade.cod_responsavel);
                    DAO.AcessoDadosMySQL.AdicionarParametros("@vchLocal", atividade.local);
                    DAO.AcessoDadosMySQL.AdicionarParametros("@intCod_tipo_ativ", atividade.cod_tipo_ativ);

                    string strSQL = "UPDATE atividade set nome = @vchNome,resumo=@vchResumo,data_ini=STR_TO_DATE(@vchData_ini,'%d/%m/%Y'),data_fim=STR_TO_DATE(@vchData_fim,'%d/%m/%Y'),carga_horaria=@vchCarga_horaria,"
                        + "ano=@vchAno,cod_responsavel=@intCod_responsavel,localizacao=@vchLocal,cod_tipo_ativ=@intCod_tipo_ativ WHERE id = @intId;";
                    objRetorno = DAO.AcessoDadosMySQL.ExecutarManipulacao(System.Data.CommandType.Text, strSQL);
                }

                int intResultado = 0;
                if (objRetorno != null)
                {
                    if (int.TryParse(objRetorno.ToString(), out intResultado))
                        return true;
                }

                return false;
            }
            catch (Exception)
            {
                return false;
            }
        }
        public bool Delete(Atividade atividade)
        {
            try
            {
                DAO.AcessoDadosMySQL.LimparParametros();

                object objRetorno = null;
                if (atividade != null)
                {
                    DAO.AcessoDadosMySQL.AdicionarParametros("@intId", atividade.id);

                    string strSQL = "DELETE FROM atividade WHERE id = @intId;";
                    objRetorno = DAO.AcessoDadosMySQL.ExecutarManipulacao(System.Data.CommandType.Text, strSQL);
                }

                int intResultado = 0;
                if (objRetorno != null)
                {
                    if (int.TryParse(objRetorno.ToString(), out intResultado))
                        return true;
                }

                return false;
            }
            catch (Exception)
            {
                return false;
            }
        }
        public List<Atividade> List()
        {
            List<Atividade> lista = new List<Atividade>();
            try
            {
                DataTable objDataTable = null;
                //Se quiser personalizar a busca
                string strSQL = "SELECT id,nome,resumo,carga_horaria,DATE_FORMAT(data_ini,'%d/%m/%Y') AS data_ini,DATE_FORMAT(data_fim,'%d/%m/%Y') AS data_fim,ano,cod_responsavel," +
                        "localizacao,cod_tipo_ativ from atividade";
                objDataTable = DAO.AcessoDadosMySQL.ExecutaConsultar(System.Data.CommandType.Text, strSQL);
                if (objDataTable.Rows.Count <= 0)

                {
                    return lista;
                }

                foreach (DataRow objLinha in objDataTable.Rows)
                {
                    Atividade objNovaAtividade = new Atividade();
                    objNovaAtividade.id = objLinha["id"] != DBNull.Value ? Convert.ToInt32(objLinha["id"]) : 0;
                    objNovaAtividade.nome = objLinha["nome"] != DBNull.Value ? Convert.ToString(objLinha["nome"]) : "";
                    objNovaAtividade.resumo = objLinha["resumo"] != DBNull.Value ? Convert.ToString(objLinha["resumo"]) : "";
                    objNovaAtividade.data_ini = objLinha["data_ini"] != DBNull.Value ? Convert.ToString(objLinha["data_ini"]) : "";
                    objNovaAtividade.data_fim = objLinha["data_fim"] != DBNull.Value ? Convert.ToString(objLinha["data_fim"]) : "";
                    objNovaAtividade.carga_horaria = objLinha["carga_horaria"] != DBNull.Value ? Convert.ToString(objLinha["carga_horaria"]) : "";
                    objNovaAtividade.ano = objLinha["ano"] != DBNull.Value ? Convert.ToString(objLinha["ano"]) : "";
                    objNovaAtividade.cod_responsavel = objLinha["cod_responsavel"] != DBNull.Value ? Convert.ToInt32(objLinha["cod_responsavel"]) : 0;
                    objNovaAtividade.local = objLinha["localizacao"] != DBNull.Value ? Convert.ToString(objLinha["localizacao"]) : "";
                    objNovaAtividade.cod_tipo_ativ = objLinha["cod_tipo_ativ"] != DBNull.Value ? Convert.ToInt32(objLinha["cod_tipo_ativ"]) : 0;

                    lista.Add(objNovaAtividade);
                }
                return lista;
            }
            catch (Exception)
            {
                return lista;
            }
        }
    }
}