﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using projeto_v0.Models;

namespace projeto_v0.Controllers
{
    public class AssinaturaController : Controller
    {
        AssinaturaDAO assinaturaDAO = new AssinaturaDAO();
        public static List<Assinatura> listaAssinatura = new List<Assinatura>();
        // GET: Assinatura
        public ActionResult Index()
        { 
            //USARÁ???
            listaAssinatura = assinaturaDAO.List();
            return View(listaAssinatura);
            //var assinaturas = from b in listaAssinatura
                         //orderby b.id
                         //select b;
            //return View(assinaturas);
        }

        // GET: Assinatura/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Assinatura/Create
        public ActionResult Create()
        {
            return View();
        }
        //DECIDIR QUAL UTILIZAR
        /*
        // POST: Banda/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                Assinatura assinatura = new Assinatura();
                assinatura.id = listaAssinatura.Count;
                assinatura.nome = collection["nome"];
                assinatura.caminho = collection["caminho"];
                assinatura.cargo = collection["cargo"];

                listaAssinatura.Add(assinatura);

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
        */

        // POST: Assinatura/Create
        [HttpPost]
        public ActionResult Create(Assinatura assinatura)
        {
            try
            {
                // TODO: Add insert logic here

                listaAssinatura.Add(assinatura);
                assinaturaDAO.Create(assinatura);
                //return RedirectToAction("Index");
                return RedirectToAction("Index").Mensagem("Assinatura registrada com sucesso!", "Sucesso");
            }
            catch
            {
                //return View();
                return View().Mensagem("Erro ao registrar assinatura!", "Erro");

            }
        }

        // GET: Assinatura/Edit/5
        public ActionResult Edit(int id)
        {

            var assinatura = listaAssinatura.Single(p => p.id == id);

            return View(assinatura);
        }

        // POST: Assinatura/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                var assinatura = listaAssinatura.Single(p => p.id == id);
                if (TryUpdateModel(assinatura))
                {
                    assinaturaDAO.Edit(assinatura);
                    //return RedirectToAction("Index");
                    return RedirectToAction("Index").Mensagem("Assinatura alterada com sucesso!", "Sucesso");

                }
                return View(assinatura);

            }
            catch
            {
                //return View();
                return View().Mensagem("Erro ao alterar Assinatura!", "Erro");

            }
        }

        // GET: Assinatura/Delete/5
        public ActionResult Delete(int id)
        {
            var assinatura = listaAssinatura.Single(p => p.id == id);
            return View(assinatura);
        }

        // POST: Assinatura/Delete/5
        [HttpPost]
        public ActionResult Delete(Assinatura assinatura)
        {
            try
            {
                listaAssinatura.Remove(assinatura);
                assinaturaDAO.Delete(assinatura);
                //return RedirectToAction("Index");
                return RedirectToAction("Index").Mensagem("Assinatura excluído com sucesso!", "Sucesso");

            }
            catch
            {
                //return View();
                return View().Mensagem("Erro ao excluir Assinatura!", "Erro");

            }
        }
        /*
        public static List<Assinatura> listaAssinatura = new List<Assinatura>{
           new Assinatura{
              id = 1,
              nome = "Anderson",
              caminho="/c",
              cargo="coodenador"
           },
            new Assinatura{
              id = 2,
              nome = "Anderson",
              caminho="/c",
              cargo="diretor",
           },           
        };*/
    }
}
