﻿using projeto_v0.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace projeto_v0.Controllers
{
    public class ParticipanteDAO
    {
        public String Create(Participante participante)
        {
            try
            {
                DAO.AcessoDadosMySQL.LimparParametros();

                object objRetorno = null;
                if (participante != null)
                {
                    DAO.AcessoDadosMySQL.AdicionarParametros("@intId", participante.id);
                    DAO.AcessoDadosMySQL.AdicionarParametros("@vchNome", participante.nome);
                    DAO.AcessoDadosMySQL.AdicionarParametros("@vchEmail", participante.email);
                    DAO.AcessoDadosMySQL.AdicionarParametros("@vchCpf", participante.cpf);

                    string strSQL = "INSERT INTO participante (id,nome,cpf,email) "+
                        "values (@intId,@vchNome,@vchCpf,@vchEmail);  SELECT LAST_INSERT_ID();";
                    objRetorno = DAO.AcessoDadosMySQL.ExecutarManipulacao(System.Data.CommandType.Text, strSQL);
                    System.Diagnostics.Debug.WriteLine("Create part: " + objRetorno);
                }

                int intResultado = 0;
                if (objRetorno != null)
                {
                    if (int.TryParse(objRetorno.ToString(), out intResultado))
                        return objRetorno.ToString();
                }

                return "";
            }
            catch (Exception)
            {
                return "";
            }

        }
        public bool Edit(Participante participante)
        {
            try
            {
                DAO.AcessoDadosMySQL.LimparParametros();

                object objRetorno = null;
                if (participante != null)
                {
                    DAO.AcessoDadosMySQL.AdicionarParametros("@intId", participante.id);
                    DAO.AcessoDadosMySQL.AdicionarParametros("@vchNome", participante.nome);
                    DAO.AcessoDadosMySQL.AdicionarParametros("@vchEmail", participante.email);
                    DAO.AcessoDadosMySQL.AdicionarParametros("@vchCpf", participante.cpf);

                    string strSQL = "UPDATE participante set nome = @vchNome, email=@vchEmail,cpf=@vchCpf WHERE id = @intId;";
                    objRetorno = DAO.AcessoDadosMySQL.ExecutarManipulacao(System.Data.CommandType.Text, strSQL);
                }

                int intResultado = 0;
                if (objRetorno != null)
                {
                    if (int.TryParse(objRetorno.ToString(), out intResultado))
                        return true;
                }

                return false;
            }
            catch (Exception)
            {
                return false;
            }
        }
        public bool Delete(Participante participante)
        {
            try
            {
                DAO.AcessoDadosMySQL.LimparParametros();

                object objRetorno = null;
                if (participante != null)
                {
                    DAO.AcessoDadosMySQL.AdicionarParametros("@intId", participante.id);

                    string strSQL = "DELETE FROM participante WHERE id = @intId;";
                    objRetorno = DAO.AcessoDadosMySQL.ExecutarManipulacao(System.Data.CommandType.Text, strSQL);
                }

                int intResultado = 0;
                if (objRetorno != null)
                {
                    if (int.TryParse(objRetorno.ToString(), out intResultado))
                        return true;
                }

                return false;
            }
            catch (Exception)
            {
                return false;
            }
        }
        public List<Participante> List()
        {

            DAO.AcessoDadosMySQL.LimparParametros();

            List<Participante> lista = new List<Participante>();
            try
            {
                DataTable objDataTable = null;
                //Se quiser personalizar a busca
                string strSQL = "SELECT id,nome,cpf,email from participante";
                objDataTable = DAO.AcessoDadosMySQL.ExecutaConsultar(System.Data.CommandType.Text, strSQL);
                if (objDataTable.Rows.Count <= 0)

                {
                    return lista;
                }

                foreach (DataRow objLinha in objDataTable.Rows)
                {
                    Participante objNovaParticipante = new Participante();
                    objNovaParticipante.id = objLinha["id"] != DBNull.Value ? Convert.ToInt32(objLinha["id"]) : 0;
                    objNovaParticipante.nome = objLinha["nome"] != DBNull.Value ? Convert.ToString(objLinha["nome"]) : "";
                    objNovaParticipante.email = objLinha["email"] != DBNull.Value ? Convert.ToString(objLinha["email"]) : "";
                    objNovaParticipante.cpf = objLinha["cpf"] != DBNull.Value ? Convert.ToString(objLinha["cpf"]) : "";

                    lista.Add(objNovaParticipante);
                }
                return lista;
            }
            catch (Exception)
            {
                return lista;
            }
        }
        public List<Participante> ListAlunosAtividade(int id)
        {

            DAO.AcessoDadosMySQL.LimparParametros();

            List<Participante> lista = new List<Participante>();
            try
            {

               // System.Diagnostics.Debug.WriteLine("ID List: " + id.ToString());
                DAO.AcessoDadosMySQL.AdicionarParametros("@stringId", id);
                DataTable objDataTable = null;
                //Se quiser personalizar a busca
                string strSQL = "SELECT p.id,p.nome,p.cpf FROM participante p, certificado c WHERE p.id=c.cod_participante AND c.cod_atividade=@stringId;";
                objDataTable = DAO.AcessoDadosMySQL.ExecutaConsultar(System.Data.CommandType.Text, strSQL);

                System.Diagnostics.Debug.WriteLine("ID Rows: " + objDataTable.Rows.Count.ToString());
                if (objDataTable.Rows.Count <= 0)

                {
                    return lista;
                }

                foreach (DataRow objLinha in objDataTable.Rows)
                {
                    Participante objNovaParticipante = new Participante();
                    objNovaParticipante.id = objLinha["id"] != DBNull.Value ? Convert.ToInt32(objLinha["id"]) : 0;
                    objNovaParticipante.nome = objLinha["nome"] != DBNull.Value ? Convert.ToString(objLinha["nome"]) : "";
                    objNovaParticipante.cpf = objLinha["cpf"] != DBNull.Value ? Convert.ToString(objLinha["cpf"]) : "";

                    lista.Add(objNovaParticipante);
                }
                return lista;
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("Exception: " + e.ToString());
                return lista;
            }
        }
        public string BuscaCPF(string cpf)
        {

            DAO.AcessoDadosMySQL.LimparParametros();

            List<Participante> lista = new List<Participante>();
            try
            {
                DataTable objDataTable = null;
                DAO.AcessoDadosMySQL.AdicionarParametros("@stringcpf", cpf);
                //Se quiser personalizar a busca
                string strSQL = "SELECT id FROM participante WHERE cpf=@stringcpf";
                objDataTable = DAO.AcessoDadosMySQL.ExecutaConsultar(System.Data.CommandType.Text, strSQL);
                
                if (objDataTable.Rows.Count <= 0)

                {
                    return "";
                }

                foreach (DataRow objLinha in objDataTable.Rows)
                {
                    Participante objNovaParticipante = new Participante();
                    objNovaParticipante.id = objLinha["id"] != DBNull.Value ? Convert.ToInt32(objLinha["id"]) : 0;

                    lista.Add(objNovaParticipante);
                }
                System.Diagnostics.Debug.WriteLine("Create part4: " + lista[0].id);
                return Convert.ToString(lista[0].id);
            }
            catch (Exception)
            {
                return "";
            }
        }
    }
}