﻿using projeto_v0.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace projeto_v0.Controllers
{
    public class AssinaturaDAO
    {
        public bool Create(Assinatura assinatura)
        {
            try
            {
                DAO.AcessoDadosMySQL.LimparParametros();

                object objRetorno = null;
                if (assinatura != null)
                {
                    DAO.AcessoDadosMySQL.AdicionarParametros("@intId", assinatura.id);
                    DAO.AcessoDadosMySQL.AdicionarParametros("@vchNome", assinatura.nome);
                    DAO.AcessoDadosMySQL.AdicionarParametros("@vchCaminho", assinatura.caminho);
                    DAO.AcessoDadosMySQL.AdicionarParametros("@vchCargo", assinatura.cargo);

                    string strSQL = "INSERT INTO Assinatura (id,nome,caminho,cargo)" +
                        " VALUES (@intId,@vchNome,@vchCaminho,@vchCargo);";
                    objRetorno = DAO.AcessoDadosMySQL.ExecutarManipulacao(System.Data.CommandType.Text, strSQL);
                }

                int intResultado = 0;
                if (objRetorno != null)
                {
                    if (int.TryParse(objRetorno.ToString(), out intResultado))
                        return true;
                }

                return false;
            }
            catch (Exception)
            {
                return false;
            }
        }
        public bool Edit(Assinatura assinatura)
        {
            try
            {
                DAO.AcessoDadosMySQL.LimparParametros();

                object objRetorno = null;
                if (assinatura != null)
                {
                    DAO.AcessoDadosMySQL.AdicionarParametros("@intId", assinatura.id);
                    DAO.AcessoDadosMySQL.AdicionarParametros("@vchNome", assinatura.nome);
                    DAO.AcessoDadosMySQL.AdicionarParametros("@vchCaminho", assinatura.caminho);
                    DAO.AcessoDadosMySQL.AdicionarParametros("@vchCargo", assinatura.cargo);

                    string strSQL = "UPDATE Assinatura set nome=@vchNome,caminho=@vchCaminho,cargo=@vchCargo"
                        + " WHERE id = @intId;";
                    objRetorno = DAO.AcessoDadosMySQL.ExecutarManipulacao(System.Data.CommandType.Text, strSQL);
                }

                int intResultado = 0;
                if (objRetorno != null)
                {
                    if (int.TryParse(objRetorno.ToString(), out intResultado))
                        return true;
                }

                return false;
            }
            catch (Exception)
            {
                return false;
            }
        }
        public bool Delete(Assinatura assinatura)
        {
            try
            {
                DAO.AcessoDadosMySQL.LimparParametros();

                object objRetorno = null;
                if (assinatura != null)
                {
                    DAO.AcessoDadosMySQL.AdicionarParametros("@intId", assinatura.id);

                    string strSQL = "DELETE FROM Assinatura WHERE id = @intId;";
                    objRetorno = DAO.AcessoDadosMySQL.ExecutarManipulacao(System.Data.CommandType.Text, strSQL);
                }

                int intResultado = 0;
                if (objRetorno != null)
                {
                    if (int.TryParse(objRetorno.ToString(), out intResultado))
                        return true;
                }

                return false;
            }
            catch (Exception)
            {
                return false;
            }
        }
        public List<Assinatura> List()
        {
            List<Assinatura> lista = new List<Assinatura>();
            try
            {
                DataTable objDataTable = null;
                //Se quiser personalizar a busca
                string strSQL = "SELECT id,nome,caminho,cargo FROM Assinatura";
                objDataTable = DAO.AcessoDadosMySQL.ExecutaConsultar(System.Data.CommandType.Text, strSQL);
                if (objDataTable.Rows.Count <= 0)

                {
                    return lista;
                }

                foreach (DataRow objLinha in objDataTable.Rows)
                {
                    Assinatura objNovoAssinatura = new Assinatura();
                    objNovoAssinatura.id = objLinha["id"] != DBNull.Value ? Convert.ToInt32(objLinha["id"]) : 0;
                    objNovoAssinatura.nome = objLinha["nome"] != DBNull.Value ? Convert.ToString(objLinha["nome"]) : "";
                    objNovoAssinatura.caminho = objLinha["caminho"] != DBNull.Value ? Convert.ToString(objLinha["caminho"]) : "";
                    objNovoAssinatura.cargo = objLinha["cargo"] != DBNull.Value ? Convert.ToString(objLinha["cargo"]) : "";
                   
                    lista.Add(objNovoAssinatura);
                }
                return lista;
            }
            catch (Exception)
            {
                return lista;
            }
        }
    }
}
