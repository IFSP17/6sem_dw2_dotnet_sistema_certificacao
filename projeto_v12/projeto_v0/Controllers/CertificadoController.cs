﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using projeto_v0.Models;
using System.IO;
using Excel;
using System.Data;

namespace projeto_v0.Controllers
{
    public class CertificadoController : Controller
    {
        CertificadoDAO certificadoDAO = new CertificadoDAO();

        ParticipanteDAO participanteDAO = new ParticipanteDAO();
        public static List<Certificado> listaCertificado = new List<Certificado>();
        public static List<CertificadoParticipante> listaCertificadoPDF = new List<CertificadoParticipante>();
        public static List<Participante> listaParticipante = new List<Participante>();
        // GET: Certificado
        public ActionResult Index()
        {
            listaCertificado = certificadoDAO.List();
            return View(listaCertificado);
        }
        public ActionResult CreatePorArquivo(int? id)
        {
            ViewBag.ativ=id;
            return View();
        }

        // GET: Certificado/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Certificado/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Certificado/Create
        [HttpPost]
        public ActionResult Create(Certificado Certificado)
        {
            try
            {
                // TODO: Add insert logic here
                listaCertificado.Add(Certificado);
                certificadoDAO.Create(Certificado);
                //return RedirectToAction("Index");
                return RedirectToAction("Index").Mensagem("Certificado registrado com sucesso!", "Sucesso");

            }
            catch
            {
                //return View();
                return View().Mensagem("Erro ao registrar Certificado!", "Erro");

            }
        }

        // GET: Certificado/Edit/5
        public ActionResult Edit(int id)
        {

            var Certificado = listaCertificado.Single(p => p.id == id);

            return View(Certificado);
        }

        // POST: Certificado/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                var Certificado = listaCertificado.Single(p => p.id == id);
                if (TryUpdateModel(Certificado))
                {
                    certificadoDAO.Edit(Certificado);
                    //return RedirectToAction("Index");
                    return RedirectToAction("Index").Mensagem("Certificado alterado com sucesso!", "Sucesso");

                }
                return View(Certificado);

            }
            catch
            {
                //return View();
                return View().Mensagem("Erro ao alterar certificado!", "Erro");

            }
        }

        // GET: Certificado/Delete/5
        public ActionResult Delete(int id)
        {
            var Certificado = listaCertificado.Single(p => p.id == id);
            return View(Certificado);
        }

        // POST: Certificado/Delete/5
        [HttpPost]
        public ActionResult Delete(Certificado Certificado)
        {
            try
            {
                listaCertificado.Remove(Certificado);
                certificadoDAO.Delete(Certificado);
                //return RedirectToAction("Index");
                return RedirectToAction("Index").Mensagem("Certificado excluído com sucesso!", "Sucesso");

            }
            catch
            {
                //return View();
                return View().Mensagem("Erro ao excluir certificado!", "Erro");
            }
        }
     
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult MatriculaAlunos(HttpPostedFileBase uploadfile, Certificado Certificado)
        {


            System.Diagnostics.Debug.WriteLine("This will be displayed in output window" + Certificado.cod_atividade.ToString());
            Participante part = new Participante();
            Certificado cert = new Certificado();
            if (ModelState.IsValid)
            {
                if (uploadfile != null && uploadfile.ContentLength > 0)
                {
                    //ExcelDataReader works on binary excel file
                    Stream stream = uploadfile.InputStream;
                    //We need to written the Interface.
                    IExcelDataReader reader = null;
                    if (uploadfile.FileName.EndsWith(".xls"))
                    {
                        //reads the excel file with .xls extension
                        reader = ExcelReaderFactory.CreateBinaryReader(stream);
                    }
                    else if (uploadfile.FileName.EndsWith(".xlsx"))
                    {
                        //reads excel file with .xlsx extension
                        reader = ExcelReaderFactory.CreateOpenXmlReader(stream);
                    }
                    else
                    {
                        //Shows error if uploaded file is not Excel file
                        ModelState.AddModelError("File", "This file format is not supported");
                        // return View();
                    }
                    //treats the first row of excel file as Coluymn Names
                    reader.IsFirstRowAsColumnNames = true;
                    //Adding reader data to DataSet()
                    DataSet result = reader.AsDataSet();
                    reader.Close();


                    foreach (DataRow row in result.Tables[0].Rows)
                    {
                        foreach (DataColumn column in result.Tables[0].Columns)
                        {
                            if (column.ColumnName.ToString() == "Nome")
                                part.nome = row[column.ColumnName].ToString();
                            else if (column.ColumnName.ToString() == "E-mail")
                                part.email = row[column.ColumnName].ToString();
                            else if (column.ColumnName.ToString() == "CPF")
                                part.cpf = row[column.ColumnName].ToString();
                        }

                        string cpfEncontrado =participanteDAO.BuscaCPF(part.cpf);
                        if (cpfEncontrado == "")
                            cert.cod_participante = Convert.ToInt16(participanteDAO.Create(part));
                        else
                            cert.cod_participante = Convert.ToInt16(cpfEncontrado);

                        cert.cod_atividade = Certificado.cod_atividade;
                        var verificaCert = certificadoDAO.BuscaCertificadoExistente(cert.cod_atividade, cert.cod_participante);
                        if(verificaCert==0)
                            certificadoDAO.Create(cert);  
                    }
                    //Sending result data to View

                }
            }
            else
            {
                ModelState.AddModelError("File", "Please upload your file");
            }
            return RedirectToAction("Index");
        }

        public ActionResult ConcluirAtividade(int id)
        {
            listaCertificado=certificadoDAO.List(id);
            ViewBag.id = id;

            return View();
        }

        [HttpPost]
        public ActionResult ConcluirAtividade(int id, FormCollection collection)
        {
            try
            {
                
                System.Diagnostics.Debug.WriteLine("Concluir" +id+" "+collection.Get(3));
                Certificado certificado = new Certificado();
                certificado.data_emissao = collection.Get(3);
                certificado.cod_template = Convert.ToInt32(collection.Get(4));
                certificado.cod_atividade = Convert.ToInt32(id);
                certificado.cod_assinatura_1 = Convert.ToInt32(collection.Get(5));
                certificado.cod_assinatura_2 = Convert.ToInt32(collection.Get(6));
                var data = certificado.data_emissao.Split('/');
                listaParticipante = participanteDAO.ListAlunosAtividade(Convert.ToInt32(id));
                for (int i = 0; i < listaParticipante.Count(); i++)
                {
                    certificado.cod_unico_certif = data[2] + certificado.cod_atividade + certificado.cod_participante;
                    certificado.cod_participante = listaParticipante[i].id;
                    certificadoDAO.CompletarCert(certificado);
                }
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
        public ActionResult AlunosMatriculados(int? id,int? tipo)
        {
            ViewBag.idCert = id;

            ViewBag.tipo = tipo;
            System.Diagnostics.Debug.WriteLine("ID: " + id.ToString());
            listaParticipante = participanteDAO.ListAlunosAtividade(Convert.ToInt16(id));
            
            return PartialView(listaParticipante);
        }
        
        public ActionResult CertificadoCancelado(int part,int? ativ,int? tipo)
        {
            try
            {
                var participante = listaParticipante.Single(p => p.id == part);
                listaParticipante.Remove(participante);
                certificadoDAO.DeleteCertificadoIndeferido(Convert.ToInt16(ativ), part);
                if(tipo==2)
                    return RedirectToAction("Detalhes","Atividade", new { id = Convert.ToInt32(ativ) });
                else
                    return RedirectToAction("ConcluirAtividade",new {id=Convert.ToInt32(ativ) });
            }
            catch(Exception ex)
            {
                return View();                

            }
        }
        public ActionResult CertificadoPDF(int id)
        {
            listaCertificadoPDF = certificadoDAO.ListCertificadoPDF(id);
            ViewBag.nome_participante = listaCertificadoPDF[0].nome_participante;
            ViewBag.resumo = listaCertificadoPDF[0].resumo;
            ViewBag.nome_atividade = listaCertificadoPDF[0].nome_atividade;
            ViewBag.carga_horaria = listaCertificadoPDF[0].carga_horaria;
            ViewBag.cod_unico_certif = listaCertificadoPDF[0].cod_unico_certif;
            ViewBag.cpf_participante = listaCertificadoPDF[0].cpf_participante;
            ViewBag.data_emissao = listaCertificadoPDF[0].data_emissao;
            ViewBag.data_fim = listaCertificadoPDF[0].data_fim;
            ViewBag.data_inicio = listaCertificadoPDF[0].data_inicio;
            ViewBag.local = listaCertificadoPDF[0].local;
            ViewBag.lista =listaCertificadoPDF[0].listAssinaturas;
            return View(listaCertificadoPDF);
        }
    }

}
