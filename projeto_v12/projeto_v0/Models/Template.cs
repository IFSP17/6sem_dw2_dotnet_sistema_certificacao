﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace projeto_v0.Models
{
    public class Template
    {
        public int id { get; set; }
        public String caminho { get; set; }
        public String desc { get; set; }
    }
}