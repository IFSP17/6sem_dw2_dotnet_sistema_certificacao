﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace projeto_v0.Models
{
    public class CertificadoParticipante
    {
        public String nome_participante { get; set; }
        public String cpf_participante { get; set; }
        public String cod_unico_certif { get; set; }
        public String nome_atividade { get; set; }
        public String data_emissao { get; set; }
        public String local { get; set; }
        public String resumo { get; set; }
        public String carga_horaria { get; set; }
        public String data_inicio { get; set; }
        public String data_fim { get; set; }
        public List<Assinatura> listAssinaturas { get; set; }
    }
}