﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace projeto_v0.Models
{
    public class Participante
    {
        public int id { get; set; }
        public String nome { get; set; }
        public String email { get; set; }
        public String cpf { get; set; }
    }
}