﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace projeto_v0.Models
{
    public class TipoAtividade
    {
        public int id { get; set; }
        public String desc { get; set; }
        public int cod_template { get; set; }
    }
}