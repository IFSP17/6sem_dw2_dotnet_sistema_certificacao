﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace projeto_v0.Models
{
    public class Certificado
    {
        public int id { get; set; }
        public String cod_unico_certif { get; set; }
        public int cod_atividade { get; set; }
        public int cod_participante { get; set; }
        public String data_emissao { get; set; }
        public int cod_template { get; set; }
        public int cod_assinatura_1 { get; set; }
        public int cod_assinatura_2 { get; set; }

    }
}