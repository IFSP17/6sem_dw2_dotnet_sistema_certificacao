﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace projeto_v0.Models
{
    public class Assinatura
    {
        public int id { get; set; }
        public String nome { get; set; }
        public String caminho { get; set; }
        public String cargo { get; set; }
    }
}