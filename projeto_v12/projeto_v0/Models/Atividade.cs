﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace projeto_v0.Models
{
    public class Atividade
    {
        public int id { get; set; }
        public String nome { get; set; }
        public String data_ini { get; set; }
        public String data_fim { get; set; }
        public String carga_horaria { get; set; }
        public String resumo { get; set; }
        public int cod_responsavel { get; set; }
        public String ano { get; set; }        
        public String local { get; set; }
        public int cod_tipo_ativ { get; set; }
    }
}