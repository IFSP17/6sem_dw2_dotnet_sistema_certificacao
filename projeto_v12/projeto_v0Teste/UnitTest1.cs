﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using projeto_v0.Controllers;
using projeto_v0.Models;

namespace projeto_v0Teste
{
    [TestClass]
    public class UnitTest1
    {
        UsuarioDAO UsuarioDAO = new UsuarioDAO();

        [TestInitialize]
        public void IniciarTestes()
        {


        }

        [TestMethod]
        public void TestCreate()
        {
            Usuario novoUsuario = new Usuario();
            novoUsuario.id = 3;
            novoUsuario.nome = "Renata";
            novoUsuario.user = "Renata";
            novoUsuario.email = "Renata@gmail.com";
            novoUsuario.senha = "123456";
            var resultado = UsuarioDAO.Create(novoUsuario);

            Assert.AreEqual(true, resultado);
        }

        [TestMethod]
        public void TestEdit()
        {
            Usuario Usuario = new Usuario();
            Usuario.id = 3;
            Usuario.email = "renata@outlook.com";
            var resultado = UsuarioDAO.Edit(Usuario);

            Assert.AreEqual(true, resultado);
        }

        [TestMethod]
        public void TestDelete()
        {
            Usuario Usuario = new Usuario();
            Usuario.id = 3;
            var resultado = UsuarioDAO.Edit(Usuario);

            Assert.AreEqual(true, resultado);

        }

        [TestMethod]
        public void TestList()
        {
            List<Usuario> listaUsuario = UsuarioDAO.List();
            Assert.IsNotNull(listaUsuario);
        }

        [TestCleanup]

        public void FinalizarTest()
        {
        }


    }
}
