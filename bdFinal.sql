-- MySQL dump 10.13  Distrib 5.7.9, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: projeto_certificado
-- ------------------------------------------------------
-- Server version	5.6.15-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `assinatura`
--

DROP TABLE IF EXISTS `assinatura`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `assinatura` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `NOME` varchar(45) NOT NULL,
  `CAMINHO` varchar(45) NOT NULL COMMENT 'Caminho para imagem escaneada da assinatura desejada\n',
  `CARGO` varchar(45) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `assinatura`
--

LOCK TABLES `assinatura` WRITE;
/*!40000 ALTER TABLE `assinatura` DISABLE KEYS */;
INSERT INTO `assinatura` VALUES (1,'Profª. Dra. Lilian M. Pino Elias','c:/teste','Coordenadora de Extensão'),(2,'Prof. Ms. Aguinaldo L. de Barros Lorandi','C:/teste','Diretor Geral');
/*!40000 ALTER TABLE `assinatura` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `atividade`
--

DROP TABLE IF EXISTS `atividade`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `atividade` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `NOME` varchar(45) NOT NULL,
  `DATA_INI` date NOT NULL,
  `DATA_FIM` date NOT NULL,
  `CARGA_HORARIA` varchar(45) NOT NULL,
  `ANO` varchar(45) NOT NULL,
  `COD_RESPONSAVEL` int(11) NOT NULL,
  `LOCALIZACAO` varchar(45) NOT NULL,
  `COD_TIPO_ATIV` int(11) NOT NULL,
  `RESUMO` varchar(1000) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `fk_Atividade_TipoAtividade_idx` (`COD_TIPO_ATIV`),
  KEY `fk_Atividade_Responsável_idx` (`COD_RESPONSAVEL`),
  CONSTRAINT `fk_Atividade_Responsável` FOREIGN KEY (`COD_RESPONSAVEL`) REFERENCES `usuario` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Atividade_TipoAtividade` FOREIGN KEY (`COD_TIPO_ATIV`) REFERENCES `tipoatividade` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `atividade`
--

LOCK TABLES `atividade` WRITE;
/*!40000 ALTER TABLE `atividade` DISABLE KEYS */;
INSERT INTO `atividade` VALUES (1,'Mini Curso de Jogos','2017-10-10','2017-10-10','20h','2017',1,'IFSP',1,'Aprendizado de Tiles, linguagem de programação C e C++'),(2,'IC','2017-10-12','0000-00-00','2hrs','2017',1,'IFSP',1,''),(3,'fggt','2018-11-11','2018-12-11','50','2018',1,'IFSP',1,'dghdhyhy');
/*!40000 ALTER TABLE `atividade` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `certificado`
--

DROP TABLE IF EXISTS `certificado`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `certificado` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `COD_UNICO_CERTIF` varchar(20) DEFAULT NULL,
  `COD_ATIVIDADE` int(11) NOT NULL,
  `COD_PARTICIPANTE` int(11) NOT NULL,
  `DATA_EMISSAO` date DEFAULT NULL,
  `COD_TEMPLATE` int(11) DEFAULT NULL,
  `COD_ASSINATURA_1` int(11) DEFAULT NULL,
  `COD_ASSINATURA_2` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `fk_Certificado_Assinatura1_idx` (`COD_ASSINATURA_1`),
  KEY `fk_Certificado_Assinatura2_idx` (`COD_ASSINATURA_2`),
  KEY `fk_Certificado_Atividade_idx` (`COD_ATIVIDADE`),
  KEY `fk_Certificado_Participante_idx` (`COD_PARTICIPANTE`),
  KEY `fk_Certificado_Template_idx` (`COD_TEMPLATE`),
  CONSTRAINT `fk_Certificado_Assinatura1` FOREIGN KEY (`COD_ASSINATURA_1`) REFERENCES `assinatura` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Certificado_Assinatura2` FOREIGN KEY (`COD_ASSINATURA_2`) REFERENCES `assinatura` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Certificado_Atividade` FOREIGN KEY (`COD_ATIVIDADE`) REFERENCES `atividade` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Certificado_Participante` FOREIGN KEY (`COD_PARTICIPANTE`) REFERENCES `participante` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Certificado_Template` FOREIGN KEY (`COD_TEMPLATE`) REFERENCES `template` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=159 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `certificado`
--

LOCK TABLES `certificado` WRITE;
/*!40000 ALTER TABLE `certificado` DISABLE KEYS */;
INSERT INTO `certificado` VALUES (22,'21',2,1,'0000-00-00',1,1,2),(143,'20171139',1,141,'2017-12-12',1,1,2),(144,'20171141',1,142,'2017-12-12',1,1,2),(145,'20171142',1,143,'2017-12-12',1,1,2),(147,'20171143',1,145,'2017-12-12',1,1,2),(148,'20171145',1,146,'2017-12-12',1,1,2),(149,'20171146',1,147,'2017-12-12',1,1,2),(150,'20171147',1,148,'2017-12-12',1,1,2),(151,'20171148',1,149,'2017-12-12',1,1,2),(153,'20171149',1,151,'2017-12-12',1,1,2),(155,'20171151',1,153,'2017-12-12',1,1,2),(157,'20171153',1,155,'2017-12-12',1,1,2),(158,'20171155',1,156,'2017-12-12',1,1,2);
/*!40000 ALTER TABLE `certificado` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary view structure for view `listagem`
--

DROP TABLE IF EXISTS `listagem`;
/*!50001 DROP VIEW IF EXISTS `listagem`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `listagem` AS SELECT 
 1 AS `NOME_ATIVIDADE`,
 1 AS `CODIGO_UNICO`,
 1 AS `NOME`,
 1 AS `CPF`,
 1 AS `DATA_EMISSAO`*/;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `participante`
--

DROP TABLE IF EXISTS `participante`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `participante` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `NOME` varchar(45) NOT NULL,
  `EMAIL` varchar(45) NOT NULL,
  `CPF` varchar(45) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=158 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `participante`
--

LOCK TABLES `participante` WRITE;
/*!40000 ALTER TABLE `participante` DISABLE KEYS */;
INSERT INTO `participante` VALUES (1,'Lidiane Laila Albrecht','lidiane@lidiane','111.111.111-11'),(135,'Afonso Angeli Christofoletti','afonsoangeli@hotmail.com','377.847.458-85'),(136,'Alexandre Alves Tavares','alexandre_alt@hotmail.com','408.398.718-99'),(137,'Alexandre Alves Tavares','alexandre_alt@hotmail.com','40839871899'),(138,'Amauri Pereira de Sousa Junior','amauripsj@gmail.com','45802464895'),(139,'Cristiano Alves Pessoa','cristianopessoa@gmail.com','282.620.898-55'),(140,'Ederson Garcia','edgarcia1975@gmail.com','25080111852'),(141,'Eraldo Gonçalves Junior','eraldogoncalvesjr@gmail.com','381.608.748-55'),(142,'Frederico Angeli Christofoletti','fred.pnk_@hotmail.com','377.847.448-03'),(143,'Gabriel Roberto Weygand de Souza','gabrielsouza.ifsp@gmail.com','313.724.088-35'),(144,'Luciano Teixeira ','Luhciano_lp@hotmail.com','38389026880'),(145,'Luiz Cavamura','lcavamura@gmail.com ','253.197.538-10'),(146,'Luiz Henrique Geromel','geromel@ifsp.edu.br','15462598807'),(147,'Wilson Marccello Peres ','marccello.peres@gmail.com','324.478.698-78'),(148,'Marcio Kassouf Crocomo','marciokc@gmail.com','325.223.598-60'),(149,'Osias Baptista de souza Filho','osiasbap@gmail.com','011.773.228-11'),(150,'Pedro Fernandes Magalhães','pedrofmquake@hotmail.com','43283375801'),(151,'Raul Socoloski','rfsock@gmail.com','865.473.728-04'),(152,'Rogério Daniel Dantas','rogerio.dantas@ifsp.edu.be','22388146801'),(153,'Sergio Giovanni Silvestre Machado','sergio.machadopk@gmail.com','396.386.588-19'),(154,'Tatiane Cristina da Costa Fernandes','taticrisfernandes@gmail.com','082821896-00'),(155,'Thiago Luís Lopes Siqueira','thiago.ifsp@gmail.com','310.603.348-79'),(156,'Victor Hugo Ferrari Canedo Radich','victor.radich@yahoo.com.br','092.660.097-41'),(157,'Teste','teste@teste','000.000.000-00');
/*!40000 ALTER TABLE `participante` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `template`
--

DROP TABLE IF EXISTS `template`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `template` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `CAMINHO` varchar(45) NOT NULL COMMENT 'Provavelmente o caminho para o template a ser usado.',
  `DESCR` varchar(90) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `template`
--

LOCK TABLES `template` WRITE;
/*!40000 ALTER TABLE `template` DISABLE KEYS */;
INSERT INTO `template` VALUES (1,'teste','teste');
/*!40000 ALTER TABLE `template` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipoatividade`
--

DROP TABLE IF EXISTS `tipoatividade`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipoatividade` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `DESCR` varchar(45) NOT NULL,
  `COD_TEMPLATE` int(11) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `fk_Tipo_Atividade_Template_idx` (`COD_TEMPLATE`),
  CONSTRAINT `fk_Tipo_Atividade_Template` FOREIGN KEY (`COD_TEMPLATE`) REFERENCES `template` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipoatividade`
--

LOCK TABLES `tipoatividade` WRITE;
/*!40000 ALTER TABLE `tipoatividade` DISABLE KEYS */;
INSERT INTO `tipoatividade` VALUES (1,'Teste1',1);
/*!40000 ALTER TABLE `tipoatividade` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuario`
--

DROP TABLE IF EXISTS `usuario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuario` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `NOME` varchar(45) NOT NULL,
  `USER` varchar(45) NOT NULL,
  `SENHA` varchar(45) NOT NULL,
  `EMAIL` varchar(45) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuario`
--

LOCK TABLES `usuario` WRITE;
/*!40000 ALTER TABLE `usuario` DISABLE KEYS */;
INSERT INTO `usuario` VALUES (1,'Renata','renata','123','renata@renata'),(2,'Teste','teste','123','teste@teste'),(3,'Carlos da Silva','carlosSilva','123','carlos@hotmail.com');
/*!40000 ALTER TABLE `usuario` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'projeto_certificado'
--

--
-- Dumping routines for database 'projeto_certificado'
--

--
-- Final view structure for view `listagem`
--

/*!50001 DROP VIEW IF EXISTS `listagem`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `listagem` AS select `ativ`.`NOME` AS `NOME_ATIVIDADE`,`certif`.`COD_UNICO_CERTIF` AS `CODIGO_UNICO`,`part`.`NOME` AS `NOME`,`part`.`CPF` AS `CPF`,`certif`.`DATA_EMISSAO` AS `DATA_EMISSAO` from ((`certificado` `certif` join `atividade` `ativ` on((`certif`.`COD_ATIVIDADE` = `ativ`.`ID`))) join `participante` `part` on((`certif`.`COD_PARTICIPANTE` = `part`.`ID`))) order by `ativ`.`NOME`,`certif`.`COD_UNICO_CERTIF`,`part`.`NOME` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-12-13  9:58:43
